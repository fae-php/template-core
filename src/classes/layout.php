<?php
namespace FAE\template;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use Twig\Loader\FilesystemLoader;
use Twig\Loader\ChainLoader;
use Twig\Environment;

use FAE\fae\frames;
use FAE\fae\fae;
use FAE\auth\auth;
use FAE\user\user;

use StdClass;

class layout {
  
  var $_variables = array();
  var $_loader;
  var $_twig;
  
  static $_routes;
  
  function __construct( StdClass $variables = null )
  {
    global $config;
    $this->_variables = $variables;
    
    
    $primaryLoader = new FilesystemLoader( [ $this->_variables->_contentPath, $this->_variables->_layoutPath ] );
    
    // Load template by path
    $templateLoader = new FilesystemLoader();
    $templateLoader->addPath( fae::_path( APPROOT, 'templates', $config->template, 'layouts' ) );
    
    // Load frames
    $frameLoader = new FilesystemLoader();
    
    $frameHandler = new frames();
    $frames = $frameHandler->loadFrames();
    
    foreach($frames as $frame => $options){
      if( property_exists( $options, 'template' ) && property_exists( $options->template, 'layout' ) ){
        $frameLoader->addPath( fae::_path( $options->path, 'src', $options->template->layout ) );
      } else if( is_dir( fae::_path( $options->path, 'src', 'views' ) ) ){
        $frameLoader->addPath( fae::_path( $options->path, 'src', 'views' ) );
      }
    }
    
    $this->_loader = new ChainLoader([$primaryLoader, $templateLoader, $frameLoader]);
    
    // Enable debugging
    $this->_twig = new Environment( $this->_loader, ['debug' => true] );
    $this->_twig->addExtension( new \Twig\Extension\DebugExtension() );
    
    // Assign global variables
    $this->_twig->addGlobal( '_get', $_GET );
    $this->_twig->addGlobal( '_url', str_replace( $config->path, '', $_SERVER['REDIRECT_URL'] ) );
    $this->_twig->addGlobal( '_config', $config->public );
    
    // Add user account as variable
    $ah = new auth();
    $auth = $ah->get();
    $user = ['isLoggedIn' => false];
    if($auth->isLoggedIn()){
      $uh = new user();
      $user = $uh->getById($auth->user_id);
      $user['isLoggedIn'] = true;
    }
    $this->_twig->addGlobal( '_user', $user );
    
    // Add functions
    $linkFunction = new \Twig_Function( 'link', function($link){
      global $config;
      if((strlen($config->path) && substr_count( $link, $config->path )) || preg_match('/^\//', $link)){
        return $link;
      }
      return $config->path.'/'.$link;
    });
    $this->_twig->addFunction($linkFunction);
    
    $userAuthFunction = new \Twig_Function( 'userVerify', function($ref, $level, $force = false){
      $auth = new \FAE\auth_local\auth();
      return $auth->verify($ref, $level, $force);
    });
    $this->_twig->addFunction($userAuthFunction);
    
    $assetFunction = new \Twig_Function( 'asset', function($asset){
      global $config;
      if( substr_count( $asset, $config->template.'/' ) ){
        return $asset;
      }
      return $config->path.'/templates/'.$config->template.'/'.$asset;
    });
    $this->_twig->addFunction($assetFunction);
    
    $dateFilter = new \Twig_Filter( 'dateSafe', function( $date, $format ){
      try {
        $dateHandle = new \DateTime($date);
        return $dateHandle->format($format);
      } catch(\Exception $e){
        return $date;
      }
    });
    $this->_twig->addFilter($dateFilter);
    
    $jsonFilter = new \Twig_Filter( 'json_decode', function( $string, $object = false ){
      try {
        $object = json_decode( $string, !$object );
        return $object;
      } catch(\Exception $e){
        return $string;
      }
    });
    $this->_twig->addFilter($jsonFilter);
    
    $isString = new \Twig_SimpleTest('string', function ($value) {
      return is_string($value);
    });
    $this->_twig->addTest($isString);
    
    $hooks = fae::$_hooks['template'];
    if(is_array($hooks['layoutFunction'])){
      foreach($hooks['layoutFunction'] as $function){
        $function = (array) $function;
        if( is_callable( current($function) ) ){
          $this->_twig->addFunction( new \Twig_Function( key($function), current($function) ) );
        }
      }
    }
    if(is_array($hooks['layoutFilter'])){
      foreach($hooks['layoutFilter'] as $name => $function){
        $function = (array) $function;
        if( is_callable( current($function) ) ){
          $this->_twig->addFilter( new \Twig_Filter( key($function), current($function) ) );
        }
      }
    }
  }
  
  function renderLayout( string $layout = null )
  {
    global $config;
    if(is_null($layout)){
      $layout = $this->_variables->_page;
    }
    echo $this->_twig->render( $layout, array_merge( (array) $this->_variables, ['config' => (array) $config ]) );
  }
  
  static function pageLoader( array $variables )
  {
    // Load data
    if($variables['_loadData']){
      $instance = new $variables['_dataController']( 'default', $variables );
      $query = $instance->get($variables);
      $variables['data'] = $query->fetchAll();
      // Load child data
      if($instance->hasChildren()){
        foreach($variables['data'] as $k => $row){
          $childrenQueries = $instance->getChildrenData( [ $variables['_method'].'_id' => $row['id'] ] );
          foreach( $childrenQueries as $childTable => $childQuery ){
            $variables['data'][$k][$childTable] = $childQuery->fetchAll();
          }
        }
      }
    }
    foreach( fae::$_hooks['template']['pageData'] as $hook ){
      if( is_callable($hook) ){
        $variables = call_user_func( $hook, $variables );
      }
    }
    // Render layout
    $layout = new self( (object) $variables );
    $layout->renderLayout();
  }
  
  static function assetLoader( array $variables )
  {
    // Load asset from non-web path
    $filePath = fae::_path( APPROOT, 'templates', $variables['template'], $variables['type'], $variables['file'] );
    if( file_exists( $filePath ) ){
      header("X-Sendfile: $filePath");
      header("Content-type: ".fae::mimeType($filePath));
      header('Content-Disposition: inline; filename="' . basename($filePath) . '"');
      header('Content-Length: '.filesize($filePath));
      fae::echofile($filePath);
    }
  }
  
  static function routeloader()
  {
    // create routes for static files
    if(self::$_routes){
      return self::$_routes;
    }
    
    self::$_routes = new RouteCollection();
    
    $route = new Route(
      '/templates/{template}/{type}/{file}',
      [
        '_controller'   => '\\'.__NAMESPACE__.'\\layout::assetLoader'
      ],
      [
        'template'      => '[a-z0-9A-Z]+',
        'type'          => '(images|js|fonts|css)',
        'file'          => '[^\/]+',
      ],
      [],
      '',
      [],
      ['GET']
    );
    self::$_routes->add( 'template-asset-loader', $route );
    
    return self::$_routes;
  }
  
}